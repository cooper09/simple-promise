# Simple Javascript Promise #

A nifty compact, simple promise from scratch. More for conceptualization than actual use.

We are free to call then() and resolve() whenever they suit our purposes. This is one of the powerful advantages of capturing the notion of eventual results into an object, The flaw in this version is we need to keep a running list of deferred callbacks inside of the Promise instead of just one.

### Uses an IIFE to define and utilize an asynchronous promise that can maintain a pending/resoved state within a self-defined object. This is not a fully active promise in that it does not provide the following: ###

* Error Handling - the REJECT function
* capturing and maintaining multiple callbacks and errors
* THEN() chaining of Promise results

### These items are left as individual exercises/branches ###

cooper s