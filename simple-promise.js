(function() {	

	function Promise(fn) {  
	 console.log('A new promdise function...'); 

	 // list of callbacks 
	  var callback = null;
	  var successCallback = null;
	  var errCallback = null;

	// list of states
	var state = 'pending';
	var value;
	var deferred;

	  function resolve(newValue) {
	  	value = newValue;
	  	console.log('promise resolved: ', value );

	  	state='resolved';
	   
	   // with a timer on the following callback will never be implemented
	   // callback(value);

	    // force callback to be called in the next
	    // iteration of the event loop, giving
	    // callback a chance to be set by then()

		if (deferred) {
			handle(deferred);
		}

	  }//end resolve function

	   function handle(onResolved) {
	    if(state === 'pending') {
	      deferred = onResolved;
	      return;
	    }

	     onResolved(value);

	   }//end handle function

	   this.then = function(onResolved) {
		    handle(onResolved);
		  };

	  fn(resolve);
	}//end Promise function 

	function doSomething() {  
	  return new Promise(function(resolve) {
	    var value = 42;
	    resolve(value);
	  });
	} //end doSomething

	//Simulate a function (any function) that is going to wait on a promise for the result
	
	var myPromise = doSomething();
	myPromise.then(function(result) {
	    console.log("got a result", result);
	});// end promise then

})();//end of Immediately Invoked Function Expression (IIFE)
